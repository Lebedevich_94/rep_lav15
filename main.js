arrow.onclick = () => window.scrollTo(pageXOffset, 0);
window.addEventListener("scroll", () => arrow.hidden = pageYOffset < document.documentElement.clientHeight);

let tab, content;
window.onload = () => {
    content = document.getElementsByClassName('content');
    tab = document.getElementsByClassName('tab');
    hiddenContent(1);
};

function hiddenContent(elem) {
    for (let i = elem; i < content.length; i++) {
        content[i].classList.remove('show');
        content[i].classList.add('hidden');
        tab[i].classList.remove('open');
    };
}
document.getElementById('tabs').onclick = (event) => {
    let target = event.target;
    if (target.className == 'tab') {
        for (let i = 0; i < tab.length; i++) {
            if (target == tab[i]) {
                showContent(i);
                break;
            };
        };
    };
};

function showContent(elem) {
    if (content[elem].classList.contains('hidden')) {
        hiddenContent(0);
        tab[elem].classList.add('open');
        content[elem].classList.remove('hidden');
        content[elem].classList.add('show');
    };
}

let pageID = document.querySelector('[data-id-page]').getAttribute('data-id-page'),
    navItem = document.querySelector(`[data-id-nav=${pageId}]`);
if (pageId == navItem.getAttribute('data-id-nav')) {
    navItem.classList.add('active');
}